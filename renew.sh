#!/bin/bash

# Renew the certificates already obtained
# docker-compose run --rm --entrypoint "\
# 	certbot renew" certbot

docker run --rm -it \
-v $PWD/data/certbot/conf:/etc/letsencrypt \
-v $PWD/data/www:/var/www/html \
certbot/certbot \
renew --webroot -w /var/www/html --quiet

# Reload certificates with nginx -s reload
docker-compose exec nginx nginx -s reload
