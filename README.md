# v2ray with Adguardhome

v2ray + nginx + tls + websocket + adguardhome 

## Installation

First, please install docker and docker-compose. You can go to [this](https://docs.docker.com/install/linux/docker-ce/ubuntu/) follow guides install.

1.Modify ```config.env``` enter your domain

```bash
domains=(domain.com)
root_path=./data
data_path=./data/certbot
nginx_path=./data/nginx
v2ray_path=./data/v2ray
rsa_key_size=4096
staging=0
email=email@gmail.com
```

2.Run ```init.sh```
```bash
chmod a+x ./init.sh
./init.sh
```

## License
[MIT](https://choosealicense.com/licenses/mit/)