#!/bin/bash

# Read config
domains=(`awk -F '=' '/domains/ {gsub(/[()]+/,"",$2); print $2}' config.env`)
email=`awk -F '=' '/email/ {print $2}' config.env`
certbot_path=$PWD/data/certbot
nginx_path=$PWD/data/nginx
webroot_path=$PWD/data/www
rsa_key_size=`awk -F '=' '/rsa_key_size/ {print $2}' config.env`
staging=`awk -F '=' '/staging/ {print $2}' config.env`

sudo apt update && sudo apt install zip unzip -y

docker network create web

if [ -z "$(docker ps -qf name="^portainer" | tail -1)" ]
then
    echo "====> Deploying Portainer ..."
    docker volume create portainer_data
    docker run -d -p 9000:9000 \
    --name portainer \
    --network web \
    --hostname portainer \
    --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v portainer_data:/data portainer/portainer
fi

if [ ! -e "$webroot_path" ]; then
  mkdir -p "$PWD/data/www"
  unzip web.zip -d $webroot_path
fi

# Generate nginx config adn v2ray config
  
rm -rf "$nginx_path"/default.conf
if [ ! -e "$nginx_path"/default.conf ]; then
  echo "====> Generate nginx config ..."
  mkdir -p "$nginx_path"
  cat > $nginx_path/default.conf <<-EOF
server {
    listen       80;
    server_name  $domains;
    root /var/www/html;
    index index.php index.html index.htm;
    location / {
        try_files \$uri \$uri/ /index.php?\$args;
    }
    location /.well-known/acme-challenge/ {
        root /var/www/html;
    }
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /etc/nginx/html;
    }
}
  
EOF
fi

# Starting nginx
echo "====> Starting nginx ..."
docker-compose up --force-recreate -d nginx
echo

sleep 5 

echo "====> Requesting Let's Encrypt certificate for $domains ..."
# Join $domains to -d args
domain_args=""
for domain in "${domains[@]}"; do
  domain_args="$domain_args -d $domain"
done

# Select appropriate email arg
case "$email" in
  "") email_arg="--register-unsafely-without-email" ;;
  *) email_arg="--email $email" ;;
esac

# Enable staging mode if needed
if [ $staging != "0" ]; then staging_arg="--staging"; fi

# Decide whether to renew the certificate
if [ -d "$certbot_path/conf/live/$domains" ]; then
  # read -p "Existing data found for $domains. Continue and replace existing certificate? (y/N) " decision
  # if [ "$decision" != "Y" ] && [ "$decision" != "y" ]; then
  #   exit
  # fi
  bash ./renew.sh
else
# Obtain certificate for specified domains
docker run --rm -it \
-v $certbot_path/conf:/etc/letsencrypt \
-v $webroot_path:/var/www/html \
certbot/certbot \
certonly --webroot -w /var/www/html \
$staging_arg \
$domain_args \
$email_arg \
--rsa-key-size $rsa_key_size \
--agree-tos \
--force-renewal
fi

echo "0 0 1 * * $PWD/renew.sh" | crontab -

sleep 1

docker-compose rm -s -f
echo

if [ ! -e "$PWD/data/adguardhome/work" ]; then
  mkdir -p "$PWD/data/adguardhome/work"
fi

bash ./gen-config.sh
echo

echo "====> Starting Service ..."
docker-compose up -d 

echo
bash ./enable-bbr.sh

echo "Done"


