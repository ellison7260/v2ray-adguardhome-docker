#!/bin/bash

helpFunction()
{
   echo ""
   echo "Usage: $0 -p wspath"
   echo -e "\t-p v2ray websocket path"
   exit 1 # Exit script after printing help
}

while getopts ":p:" opt
do
   case "$opt" in
      p ) wspath="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

if [ -z "$wspath" ]
then
	wspath=$(cat /dev/urandom | head -1 | md5sum | head -c 4)
fi

# Read config
domain=(`awk -F '=' '/domains/ {gsub(/[()]+/,"",$2); print $2}' config.env`)
nginx_path=$PWD/data/nginx
v2ray_path=$PWD/data/v2ray
uuid=$(cat /proc/sys/kernel/random/uuid)

# Generate nginx config adn v2ray config
rm -rf "$nginx_path"/default.conf
if [ ! -e "$nginx_path"/default.conf ]; then
  echo "====> Generate new nginx config ..."
  cat > $nginx_path/default.conf <<-EOF
upstream portainer
{
    server portainer:9000;
}

upstream adguardhome
{
    server 10.1.0.4:80;
}

server {
    listen       80;
    server_name  $domain;

    location /.well-known/acme-challenge/ {
        root /var/www/html;
    }
    
    location / {
        return 301 https://\$host\$request_uri;
    }

}

server {
    listen 443 ssl http2;
    server_name $domain;
    root /var/www/html;
    index index.php index.html;
    ssl_certificate /etc/letsencrypt/live/$domain/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/$domain/privkey.pem;
    #TLS Version
    ssl_protocols   TLSv1 TLSv1.1 TLSv1.2 TLSv1.3;
    ssl_ciphers     'TLS13-AES-256-GCM-SHA384:TLS13-CHACHA20-POLY1305-SHA256:TLS13-AES-128-GCM-SHA256:TLS13-AES-128-CCM-8-SHA256:TLS13-AES-128-CCM-SHA256:EECDH+CHACHA20:EECDH+CHACHA20-draft:EECDH+ECDSA+AES128:EECDH+aRSA+AES128:RSA+AES128:EECDH+ECDSA+AES256:EECDH+aRSA+AES256:RSA+AES256:EECDH+ECDSA+3DES:EECDH+aRSA+3DES:RSA+3DES:!MD5';
    ssl_prefer_server_ciphers   on;
    # Enable 1.3 0-RTT
    ssl_early_data  on;
    ssl_stapling on;
    ssl_stapling_verify on;
    #add_header Strict-Transport-Security "max-age=31536000";
    #access_log /var/log/nginx/access.log combined;
    location /$wspath {
        proxy_redirect off;
        proxy_pass http://10.1.0.3:11234;
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host \$http_host;
    }
    location / {
        try_files \$uri \$uri/ /index.php?\$args;
    }
    
    location /docker/ {
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection "upgrade";

        proxy_pass http://portainer/;
        proxy_redirect off;
    }

    location /ad/ {
        rewrite ^/$ /ad/ break;
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection "upgrade";

        proxy_pass http://adguardhome/;
        proxy_redirect ~(^http://adguardhome|^)/(.*)$ /ad/$2;
    }
}

EOF
fi

rm -rf "$v2ray_path"/config.json
if [ ! -e "$v2ray_path"/config.json ]; then
  echo "====> Generate v2ray config ..."
  mkdir -p "$v2ray_path"
  cat > $v2ray_path/config.json <<-EOF
{
  "log" : {
    "access": "/var/log/v2ray/access.log",
    "error": "/var/log/v2ray/error.log",
    "loglevel": "warning"
  },
  "inbound": {
    "port": 11234,
    "listen":"0.0.0.0",
    "protocol": "vmess",
    "settings": {
      "clients": [
        {
          "id": "$uuid",
          "level": 1,
          "alterId": 64
        }
      ]
    },
     "streamSettings": {
      "network": "ws",
      "wsSettings": {
         "path": "/$wspath"
        }
     }
  },
  "outbound": {
    "protocol": "freedom",
    "settings": {
      "domainStrategy": "UseIP"
    }
  },
  "dns": {
    "servers": [
      "10.1.0.4"
    ]
  }
}
EOF
fi

rm -rf ./v2rayconfig
cat > v2rayconfig.txt <<-EOF
address: $domain
port: 443
uuid: $uuid
alterId: 64
level: 1
security: auto
network: ws
websocket_path: /$wspath
EOF

echo "--------- v2ray Config ----------"
cat v2rayconfig.txt
